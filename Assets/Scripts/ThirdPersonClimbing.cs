﻿using System;
using UnityEngine;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(Rigidbody))]
public class ThirdPersonClimbing : MonoBehaviour
{
    [SerializeField] private Vector3 m_CenterOffset;
    [SerializeField] private LayerMask m_ClimbingLayer;
    [SerializeField] private bool m_DrawDebug;

    private const float k_ForwardDistanceMax = 1.5f;
    private const float k_HeightDistanceMax = 2.0f;

    private static readonly int m_AnimatorHanging = Animator.StringToHash("Hanging");

    private Animator m_Animator;
    private bool m_ForwardHit;
    private RaycastHit m_ForwardHitInfo;
    private bool m_HeightHit;
    private RaycastHit m_HeightHitInfo;
    private Rigidbody m_Rigidbody;
    private RigidbodyConstraints m_InitialConstraints;

    public bool isClimbing { get; private set; } = false;

    public void Drop()
    {
        isClimbing = false;
        m_Animator.SetBool(m_AnimatorHanging, isClimbing);
        m_Rigidbody.constraints = m_InitialConstraints;
        m_Rigidbody.useGravity = true;

        var position = transform.position;
        position.y -= 0.4f;
        transform.position = position;
    }

    public bool GrabLedge()
    {
        if (!m_ForwardHit || !m_HeightHit) return false;

        var centerPosition = transform.position + m_CenterOffset;

        if (!(centerPosition.y - m_HeightHitInfo.point.y <= 0.0f) ||
            !(centerPosition.y - m_HeightHitInfo.point.y >= -0.5f)) return false;
        isClimbing = true;
        m_Animator.SetBool(m_AnimatorHanging, isClimbing);
        m_Rigidbody.constraints = RigidbodyConstraints.FreezeAll;
        m_Rigidbody.useGravity = false;

        var forwardNormal = m_ForwardHitInfo.normal;
        forwardNormal.x *= 0.3f;
        forwardNormal.z *= 0.3f;

        var position = new Vector3(forwardNormal.x + m_ForwardHitInfo.point.x, m_HeightHitInfo.point.y - 1.2f,
            forwardNormal.z + m_ForwardHitInfo.point.z);

        transform.position = position;
        transform.rotation = Quaternion.FromToRotation(Vector3.back, m_ForwardHitInfo.normal);

        return true;
    }

    private void OnDrawGizmos()
    {
        if (!m_DrawDebug) return;
        var centerPosition = transform.position + m_CenterOffset;

        var forward = transform.forward;
        var up = transform.up;

        var down = -up;
        var heightPosition = centerPosition + transform.forward * 0.7f + up * k_HeightDistanceMax;

        Gizmos.color = Color.red;

        // Forward debug
        Gizmos.DrawRay(centerPosition, forward * k_ForwardDistanceMax);
        Gizmos.DrawWireSphere(centerPosition, 0.2f);
        Gizmos.DrawWireSphere(centerPosition + forward * k_ForwardDistanceMax, 0.2f);

        // Height debug
        Gizmos.DrawRay(heightPosition, down * k_HeightDistanceMax);
        Gizmos.DrawWireSphere(heightPosition, 0.2f);
        Gizmos.DrawWireSphere(heightPosition + down * k_HeightDistanceMax, 0.2f);

        Gizmos.color = Color.green;

        // Forward hit debug
        if (m_ForwardHit)
        {
            Gizmos.DrawRay(centerPosition, forward * m_ForwardHitInfo.distance);
            Gizmos.DrawWireSphere(centerPosition, 0.2f);
            Gizmos.DrawWireSphere(centerPosition + forward * m_ForwardHitInfo.distance, 0.2f);
        }

        // Height hit debug
        if (m_HeightHit)
        {
            Gizmos.DrawRay(heightPosition, down * m_HeightHitInfo.distance);
            Gizmos.DrawWireSphere(heightPosition, 0.2f);
            Gizmos.DrawWireSphere(heightPosition + down * m_HeightHitInfo.distance, 0.2f);
        }
    }

    private void Start()
    {
        m_Animator = GetComponent<Animator>();
        m_Rigidbody = GetComponent<Rigidbody>();
        m_InitialConstraints = m_Rigidbody.constraints;
    }

    public void TraceForward()
    {
        var centerPosition = transform.position + m_CenterOffset;

        m_ForwardHit = Physics.SphereCast(centerPosition, 0.2f, transform.forward, out m_ForwardHitInfo,
            k_ForwardDistanceMax,
            m_ClimbingLayer.value);
    }

    public void TraceHeight()
    {
        var centerPosition = transform.position + m_CenterOffset;

        var up = transform.up;

        var heightPosition = centerPosition + transform.forward * 0.7f + up * k_HeightDistanceMax;

        m_HeightHit = Physics.SphereCast(heightPosition, 0.2f, -up, out m_HeightHitInfo, k_HeightDistanceMax,
            m_ClimbingLayer.value);
    }
}